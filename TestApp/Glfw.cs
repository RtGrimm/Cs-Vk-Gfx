using System;
using System.Linq;
using Vulkan;

using Vulkan.Linux;

using System.Runtime.InteropServices;
using System.Reflection;
using System.Collections.Generic;
using System.IO;

namespace Test
{
	class Glfw
	{
		private static readonly int GLFW_CLIENT_API = 0x00022001;
		private static readonly int GLFW_NO_API = 0;

		const string DllName = "libglfw.so";

		[DllImport (DllName)]
		private static extern IntPtr glfwInit ();

		[DllImport (DllName)]
		private static extern void glfwWindowHint (int hint, int value);

		[DllImport (DllName)]
		private static extern IntPtr glfwCreateWindow (
			int width, int height, string title, IntPtr monitor, IntPtr share);

		[DllImport (DllName)]
		private static extern bool glfwWindowShouldClose (IntPtr window);

		[DllImport (DllName)]
		private static extern void glfwPollEvents ();

		[DllImport (DllName)]
		private static extern int glfwVulkanSupported ();

		[DllImport (DllName)]
		private static extern uint glfwCreateWindowSurface (IntPtr instance, IntPtr window, IntPtr allocator, out IntPtr surface);

		[DllImport (DllName)]
		private static extern IntPtr glfwGetRequiredInstanceExtensions (out UInt32 count);

		[DllImport (DllName)]
		private static extern IntPtr glfwSetFramebufferSizeCallback (IntPtr window, [MarshalAs (UnmanagedType.FunctionPtr)]Action<IntPtr, int, int> callback);

		private static string[] GetInstanceExtensions ()
		{
			UInt32 count;

			unsafe {
				char** ptr = (char**)Glfw.glfwGetRequiredInstanceExtensions (out count);
				var values = new string[count];

				for (int i = 0; i < count; i++) {
					values [i] = Marshal.PtrToStringAnsi ((IntPtr)(*(ptr + i)));
				}

				return values;
			}
		}

		public static List<string> InstanceExtensions {
			get;
			private set;
		}

		public static bool VulkanSupported {
			get;
			private set;
		}

		static Glfw ()
		{
			Glfw.glfwInit ();
			InstanceExtensions = GetInstanceExtensions ().ToList ();
			VulkanSupported = glfwVulkanSupported () == 1;
		}

		public static void PollEvents ()
		{
			Glfw.glfwPollEvents ();
		}

		class WrapperHelper
		{
			public static IntPtr GetInternalPtr (object vulkanObject)
			{
				var value = (vulkanObject
					.GetType ()
					.GetField ("m", BindingFlags.NonPublic | BindingFlags.Instance)
					.GetValue (vulkanObject));

				return (IntPtr)value;

			}

			public static void SetInternalPtr (object vulkanObject, IntPtr value)
			{
				vulkanObject
					.GetType ()
					.GetField ("m", BindingFlags.NonPublic | BindingFlags.Instance)
					.SetValue (vulkanObject, (UInt64)value.ToInt64 ());
			}

		}

		public class Window
		{
			private readonly IntPtr _Window;

			public event Action<int, int> Resize = (width, height) => {};

			private static Dictionary<IntPtr, Action<int, int>> _CallbackMapping = 
				new Dictionary<IntPtr, Action<int, int>> ();


			static Window ()
			{
				
			}

			public Window (int width, int height, string name)
			{
				

				Glfw.glfwWindowHint (Glfw.GLFW_CLIENT_API, Glfw.GLFW_NO_API);

				_Window = Glfw.glfwCreateWindow (
					width, height, name, IntPtr.Zero, IntPtr.Zero);

				Glfw.glfwSetFramebufferSizeCallback (_Window, SizeChangedCallback);

				_CallbackMapping [_Window] = (newWidth, newHeight) => 
					Resize (newWidth, newHeight);

			}

			static void SizeChangedCallback (IntPtr window, int width, int height)
			{
				_CallbackMapping [window] (width, height);
			}

			public bool ShouldClose ()
			{
				return Glfw.glfwWindowShouldClose (_Window);
			}



			public SurfaceKhr CreateSurface (Instance instance)
			{
				IntPtr surfacePtr;

				var instancePtr = WrapperHelper.GetInternalPtr (instance);

				var surfaceResult = (Result)glfwCreateWindowSurface (
					                    instancePtr, _Window, IntPtr.Zero, out surfacePtr);

				if (surfaceResult != Result.Success) {
					throw new Exception ("Invalid Vulkan result:" + surfaceResult.ToString ());
				}

				var surface = new SurfaceKhr ();

				WrapperHelper.SetInternalPtr (surface, surfacePtr);

				return surface;
			}
		}
	}
	
}
