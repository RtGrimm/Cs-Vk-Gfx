﻿using System;
using Test;
using Gfx.Vk;
using Gfx;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using System.Runtime.InteropServices;

namespace Demos
{
	public class MainDemo : TestApp.IDemo
	{

		public Gfx.Vk.VulkanPlatform Platform {
			get;
			set;
		}


		public Gfx.IRenderTarget RenderTarget {
			get;
			set;
		}

		Geometry geometry1;

		Geometry geometry2;

		Geometry geometry3;

		Image image1;

		Image image;

		LinearGradient brush1;

		SolidBrush brush2;

		RadialGradient brush3;


		public static IEnumerable<Vec2> GenerateCircle (int pointCount, float radiusX, float radiusY)
		{
			for (int i = 0; i < pointCount; i++) {
				float theta = ((float)i / (float)pointCount) * (float)Math.PI * 2;

				yield return new Vec2 ((float)Math.Cos (theta) * radiusX, (float)Math.Sin (theta) * radiusY);
			}
		}

		
		public static Image LoadImage (IPlatformFactory factory, string path)
		{
			var bitmap = factory.CreateBitmap ();
			var image = new Image {
				Bitmap = bitmap
			};

			var bitmapFile = new System.Drawing.Bitmap (path);

			var data = bitmapFile.LockBits (
				           new System.Drawing.Rectangle (new System.Drawing.Point (0, 0), 
					           new System.Drawing.Size (bitmapFile.Width, bitmapFile.Height)), 
				           System.Drawing.Imaging.ImageLockMode.ReadOnly, 
				           System.Drawing.Imaging.PixelFormat.Format32bppArgb);

			byte[] rawImageData = new byte[data.Stride * data.Height];
			Marshal.Copy (data.Scan0, rawImageData, 0, data.Stride * data.Height);
			bitmapFile.UnlockBits (data);

			bitmap.Update (bitmapFile.Width, bitmapFile.Height, rawImageData);

			return image;

		}

		public void Setup ()
		{
			image = LoadImage (Platform.Factory (), "/home/ryan/Desktop/SR.png");
			image1 = LoadImage (Platform.Factory (), "/home/ryan/Desktop/RA.png");


		

			geometry1 = Platform.Factory ().CreateGeometry ();
			geometry2 = Platform.Factory ().CreateGeometry ();
			geometry3 = Platform.Factory ().CreateGeometry ();

			brush2 = Platform.Factory ().CreateSolidBrush ();


			brush1 = Platform.Factory ().CreateLinearGradientBrush ();
			brush1.End = new Vec2 (1, 1);

			brush1.Stops.Add (new GradientStop {
				Color = new Color (0, 0, 1, 1),
				Offset = 0
			});

			brush1.Stops.Add (new GradientStop {
				Color = new Color (1, 0, 1, 1),
				Offset = 1
			});

			brush3 = Platform.Factory ().CreateRadialGradientBrush ();
			brush3.Center = new Vec2 (0, 0);
			brush3.Radius = new Vec2 (1, 1);

			brush3.Stops.Add (new GradientStop {
				Color = new Color (0, 1, 1, 1),
				Offset = 0
			});

			brush3.Stops.Add (new GradientStop {
				Color = new Color (0, 0, 1, 1),
				Offset = 1
			});

			var rectPath = new Gfx.Path {
				Polylines = new List<Gfx.Polyline> {
					new Gfx.Polyline {
						Points = new List<Vec2> {
							new Vec2 (0, 0),
							new Vec2 (1, 0),
							new Vec2 (1, 1),
							new Vec2 (0, 1)
						}.Select (item => new Vec2 ((item.X * 0.1f) - 0.5f, (item.Y * 0.1f) - 0.5f)).ToList ()
					}
				}
			};

			var ellipsePath = new Gfx.Path {
				Polylines = new List<Gfx.Polyline> {
					new Gfx.Polyline {
						Points = GenerateCircle (50, 0.1f, 0.1f)
							.Select (item => new Vec2 ((item.X) + 0.5f, (item.Y) + 0.5f)).ToList ()
					}
				}
			};

			geometry2.Tesselate (rectPath);

			geometry3.Tesselate (ellipsePath, new TessellationInfo {
				IsStroke = true,
				StrokeWidth = 0.05f
			});

			geometry1.Tesselate (ellipsePath);
		}

		float offset = 0;

		public void Loop ()
		{
			var sinOffset = (float)Math.Sin (offset);


			brush2.Color = new Color (sinOffset, 0, 0, 1);
			brush1.Stops [0].Color = new Color (0, 0, -sinOffset, 1);
			brush3.Stops [0].Color = new Color (0, 0, sinOffset, 1);

			offset += 0.0005f;


			image.Transform = Matrix4.CreateScale (0.4f) * Matrix4.CreateTranslation (
				new Vector3 (sinOffset / 4 + 0.1f, 0, 0));

			image1.Transform = Matrix4.CreateScale (0.4f) * Matrix4.CreateTranslation (
				new Vector3 (-sinOffset / 4 - 0.1f, 0, 0));

			Platform.Renderer ().Render (new RenderInfo {
				ClearColor = new Color (0, 0.5f, 0.7f, 1),
				Target = RenderTarget,
				RenderObjects = new List<IRenderObject> {
					new Shape () {
						Geometry = geometry1,
						Fill = brush1,
						Transform = Matrix4.CreateTranslation (
							new Vector3 (sinOffset / 2, 0, 0))
					},
					new Shape () {
						Geometry = geometry2,
						Fill = brush2, 
						Transform = Matrix4.CreateTranslation (
							new Vector3 (-sinOffset / 2, 0, 0))
					},
					new Shape () {
						Geometry = geometry3,
						Fill = brush3,
						Transform = Matrix4.CreateTranslation (
							new Vector3 (0, sinOffset / 2, 0))
					},
					image,
					image1
				}
			});
		}

	
		public MainDemo ()
		{
		}
	}
}

