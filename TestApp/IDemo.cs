using System;
using Test;
using Gfx.Vk;
using Gfx;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using System.Runtime.InteropServices;

namespace TestApp
{

	interface IDemo
	{
		VulkanPlatform Platform {
			get;
			set;
		}

		Gfx.IRenderTarget RenderTarget {
			get;
			set;
		}

		void Setup ();

		void Loop ();
	}
	
}
