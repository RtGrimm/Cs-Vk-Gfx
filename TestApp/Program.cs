﻿using System;
using Test;
using Gfx.Vk;
using Gfx;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using System.Runtime.InteropServices;
using Demos;

namespace TestApp
{


	class App
	{
		Glfw.Window _Window;

		public App ()
		{
			_Window = new Glfw.Window (1200, 800, "Vulkan");
		}

	
		public void Run (IDemo demo)
		{
			

			VulkanPlatform platform = new VulkanPlatform (Glfw.InstanceExtensions);


			var renderTarget = platform.CreateScreenRenderTarget (
				                   instance => _Window.CreateSurface (instance), false);

			demo.Platform = platform;
			demo.RenderTarget = renderTarget;

			demo.Setup ();


			float offset = 0;

			while (!_Window.ShouldClose ()) {

				var startTime = DateTime.Now;

				demo.Loop ();

				Console.WriteLine ((DateTime.Now - startTime).TotalMilliseconds);

				Glfw.PollEvents ();
			}
		}
	}

	class MainClass
	{
		public static void Main (string[] args)
		{
			var app = new App ();
			app.Run (new MainDemo ());
		}
	}
}
