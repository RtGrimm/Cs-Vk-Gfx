#version 450

#define MAX_IMAGE_COUNT 500

layout(location = 0) out vec4 Color;

layout(set = 0, binding = 1) uniform sampler2D Textures[MAX_IMAGE_COUNT];

in VertexData {
  vec2 UV;

} VertData;

flat in int Index;

void main() {
  Color = texture(Textures[Index], VertData.UV);
  //Color = vec4(1, 1, 1, 1);
}
