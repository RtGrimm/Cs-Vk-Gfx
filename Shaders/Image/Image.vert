#version 450

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) in vec3 Location;
layout(location = 1) in vec2 UV;

out VertexData {
  vec2 UV;
  int ImageIndex;
} VertData;

struct ImageData_t {
  mat4 Transform;
  int ImageIndex;
  int _Padding, __Padding, ___Padding;
};

layout(set = 0, binding = 0) buffer ImageDataBuffer {
  ImageData_t DataList[];
} ImageData;

flat out int Index;

void main() {
  ImageData_t imageData = ImageData.DataList[gl_InstanceIndex];

  Index = imageData.ImageIndex;

  VertData.UV = UV;
  gl_Position = imageData.Transform * vec4( Location, 1.0 );
}
