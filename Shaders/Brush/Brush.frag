#version 450

struct BrushData {
  vec4 Colors[4];
  float Offsets[4];

  vec4 Vector;

  int StopCount;
  int BrushType;

	int _Padding, __Padding;
};

layout(location = 0) out vec4 Color;

layout(set = 0, binding = 1) buffer BrushDataBuffer {
  BrushData Items[];
} BrushDataList;

layout(push_constant) uniform PushConstantDataBuffer {
  int TransformIndex;
  int BrushIndex;
} PushConstantData;

in VertexData {
  vec2 UV;
} VertData;



vec4 GenerateGradient(float offset, BrushData data) {
		vec4 color = vec4(0, 0, 0, 0);

		for(int i = 1; i < data.StopCount; i++) {
		   float lowerOffset = data.Offsets[i - 1];
		   float upperOffset = data.Offsets[i];

		   float localOffset = (offset - lowerOffset)
						  / (upperOffset - lowerOffset);

		   vec4 newColor = mix(data.Colors[i - 1], data.Colors[i], localOffset);
		   color = mix(color, newColor, float(offset >= lowerOffset && offset <= upperOffset));
		}

		color = mix(color, data.Colors[data.StopCount - 1],
					float(offset >= data.Offsets[data.StopCount - 1]));

        color = mix(color, data.Colors[0],
					float(offset <= data.Offsets[0]));

		return color;
	}

vec4 LinearGradient(BrushData data) {
    vec2 startPoint = data.Vector.xy;
		vec2 endPoint = data.Vector.zw;

		vec2 v = endPoint - startPoint;
		float d = length(v);
		v = normalize(v);

		vec2 v0 = VertData.UV - startPoint;
		float offset = dot(v0, v);
		offset = offset / d;

    return GenerateGradient(offset, data);
}

vec4 RadialGradient(BrushData data) {
    vec2 center = data.Vector.xy;
    vec2 radius = data.Vector.zw;

    float offset = length(
      (VertData.UV - center) / radius);

    return GenerateGradient(offset, data);
}



void main() {
  BrushData data = BrushDataList.Items[PushConstantData.BrushIndex];

  if(data.BrushType == 0) {
    Color = data.Colors[0];
  }

  if(data.BrushType == 1) {
    Color = LinearGradient(data);
  }

  if(data.BrushType == 2) {
    Color = RadialGradient(data);
  }


}
