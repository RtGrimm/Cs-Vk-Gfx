#version 450

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) in vec3 Location;
layout(location = 1) in vec2 UV;

out VertexData {
  vec2 UV;
} VertData;

layout(set = 0, binding = 0) buffer ShapeDataBuffer {
  mat4 Transform[];
} ShapeData;

layout(push_constant) uniform PushConstantDataBuffer {
  int TransformIndex;
  int BrushIndex;
} PushConstantData;

void main() {
  VertData.UV = UV;
  gl_Position = ShapeData.Transform[PushConstantData.TransformIndex] * vec4( Location, 1.0 );
}
