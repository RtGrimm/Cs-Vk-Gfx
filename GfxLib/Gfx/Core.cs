﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using LibTessDotNet;
using System.Linq;
using OpenTK;

namespace Gfx
{
	[StructLayoutAttribute (LayoutKind.Sequential)]
	public struct Color
	{
		public float R, G, B, A;

		public Color (float r, float g, float b, float a)
		{
			this.R = r;
			this.G = g;
			this.B = b;
			this.A = a;
		}

	}

	[StructLayout (LayoutKind.Sequential)]
	public struct Vec2
	{
		public float X, Y;

		public Vec2 (float x, float y)
		{
			this.X = x;
			this.Y = y;
		}
	}

	[StructLayout (LayoutKind.Sequential)]
	public struct Vec3
	{
		public float X, Y, Z;

		public Vec3 (float x, float y, float z)
		{
			this.X = x;
			this.Y = y;
			this.Z = z;
		}
	}

	[StructLayout (LayoutKind.Sequential)]
	public struct Vec4
	{
		public float X, Y, Z, W;

		public Vec4 (float x, float y, float z, float w)
		{
			this.X = x;
			this.Y = y;
			this.Z = z;
			this.W = w;
		}
	}

	public class InvalidationBase
	{
		public bool Invalid {
			get;
			private set;
		}

		public InvalidationBase ()
		{
		}

		public void Invalidate ()
		{
			Invalid = true;
		}

		public void Validate ()
		{
			Invalid = false;
		}
	}


	public abstract class Brush : InvalidationBase
	{
	}

	public class GradientStop
	{
		public Color Color {
			get;
			set;
		}

		public float Offset {
			get;
			set;
		}
	}

	public abstract class GradientBrush : Brush
	{
		List<GradientStop> _Stops;

		public List<GradientStop> Stops {
			get {
				return _Stops;
			}
			set {
				_Stops = value;
				Invalidate ();
			}
		}

		public GradientBrush ()
		{
			this.Stops = new List<GradientStop> ();
		}
		
	}

	public abstract class RadialGradient : GradientBrush
	{
		Vec2 _Center;

		public Vec2 Center {
			get {
				return _Center;
			}
			set {
				_Center = value;
				Invalidate ();
			}
		}

		Vec2 _Radius;

		public Vec2 Radius {
			get {
				return _Radius;
			}
			set {
				_Radius = value;
				Invalidate ();
			}
		}

	}

	public abstract class LinearGradient : GradientBrush
	{
		Vec2 _Start;

		public Vec2 Start {
			get {
				return _Start;
			}
			set {
				_Start = value;
				Invalidate ();
			}
		}

		Vec2 _End;

		public Vec2 End {
			get {
				return _End;
			}
			set {
				_End = value;
				Invalidate ();
			}
		}
	}

	public abstract class SolidBrush : Brush
	{
		Color _Color;

		public Color Color {
			get {
				return this._Color;
			}
			set {
				_Color = value;
				Invalidate ();
			}
		}
	}

	public abstract class Bitmap : InvalidationBase, IDisposable
	{
		public abstract void Dispose ();

		public virtual void Update (int width, int height, byte[] data)
		{
			Invalidate ();
		}
	}

	public interface IRenderObject
	{
		
	}

	public class Shape : InvalidationBase, IRenderObject
	{
		OpenTK.Matrix4 _Transform;

		public OpenTK.Matrix4 Transform {
			get {
				return _Transform;
			}
			set {
				_Transform = value;
				Invalidate ();
			}
		}



		public Geometry Geometry {
			get;
			set;
		}

		public Brush Fill {
			get;
			set;
		}


		public Shape ()
		{
			Transform = OpenTK.Matrix4.Identity;
		}
	}

	public class Image : InvalidationBase, IRenderObject
	{
		public Bitmap Bitmap {
			get;
			set;
		}

		OpenTK.Matrix4 _Transform;

		public OpenTK.Matrix4 Transform {
			get {
				return _Transform;
			}
			set {
				_Transform = value;
				Invalidate ();
			}
		}


		public Image ()
		{
			_Transform = Matrix4.Identity;
		}
	}

	public class RenderInfo
	{
		public List<IRenderObject> RenderObjects {
			get;
			set;
		} = new List<IRenderObject>();

		public IRenderTarget Target {
			get;
			set;
		}

		public Color ClearColor {
			get;
			set;
		}

		
	}

	public interface IRenderer
	{
		void Render (RenderInfo renderInfo);
	}

	public interface IPlatformFactory
	{
		RadialGradient CreateRadialGradientBrush ();

		Geometry CreateGeometry ();

		SolidBrush CreateSolidBrush ();

		LinearGradient CreateLinearGradientBrush ();

		Bitmap CreateBitmap ();
	}

	public interface IPlatform
	{
		IPlatformFactory Factory ();

		IRenderer Renderer ();
	}

	public interface IRenderTarget : IDisposable
	{
		int Width {
			get;
		}

		int Height {
			get;
		}

		void Resize (int width, int height);
	}


}

