﻿using System;
using System.Runtime.InteropServices;

namespace Gfx.Vk
{
	class MemoryHelper
	{
		[DllImport ("libc")]
		private static unsafe extern void* memcpy (void* dest, void* src, uint count);

		public static unsafe void Copy (void* dest, void* src, uint byteCount)
		{
			memcpy (dest, src, byteCount);	
		}

		public static int SizeOfArray<T> (T[] array)
		{
			return Marshal.SizeOf<T> () * array.Length;
		}
	}
}

