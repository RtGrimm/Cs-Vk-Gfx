using System;
using System.Linq;
using Vulkan;

using Vulkan.Linux;

using System.Runtime.InteropServices;
using System.Reflection;
using System.Collections.Generic;
using System.IO;

namespace Gfx.Vk
{

	class Context : IDisposable
	{
		public Device Device {
			get;
			private set;
		}

		public Instance Instance {
			get;
			private set;
		}

		public PhysicalDevice PhysicalDevice {
			get;
			private set;
		}

		public uint RenderQueueFamilyIndex {
			get;
			private set;
		}


		private readonly bool _ValidationEnabled;
		private readonly List<string> _InstanceExtensions;
		private readonly string[] _Layers;

		public Context (bool validationEnabled, List<string> instanceExtensions)
		{
			this._ValidationEnabled = false;
			this._InstanceExtensions = instanceExtensions;

			this._Layers = _ValidationEnabled ? 
				new [] { "VK_LAYER_LUNARG_standard_validation" } : new string[] { };

			CreateInstance ();
			GetPhysicalDevice ();
			CreateDevice ();
		}



		private void CreateInstance ()
		{
			var extensions = new List<string> ();
		
			if (_ValidationEnabled) {
				extensions.Add ("VK_EXT_debug_report");
			}

			foreach (var ext in _InstanceExtensions) {
				extensions.Add (ext);
			}

			Instance = new Instance (new InstanceCreateInfo {
				EnabledLayerNames = _Layers,
				EnabledLayerCount = (uint)_Layers.Length,
				EnabledExtensionNames = extensions.ToArray ()
			});
		}

		public void Dispose ()
		{
			Device.Destroy ();
			Instance.Destroy ();
		}

		private void CreateDevice ()
		{
			string[] extentions = new String[] {
				"VK_KHR_swapchain"
			};

			Device = PhysicalDevice.CreateDevice (new DeviceCreateInfo {
				EnabledLayerNames = _Layers,
				EnabledLayerCount = (uint)_Layers.Length,
				EnabledExtensionCount = (uint)extentions.Length,
				EnabledExtensionNames = extentions,
				QueueCreateInfos = new[] {
					new DeviceQueueCreateInfo {
						QueueCount = 1,
						QueueFamilyIndex = 0,
						QueuePriorities = new[] {
							1.0f
						}
					}
				}
			});
		}

		private void GetPhysicalDevice ()
		{
			PhysicalDevice = Instance
				.EnumeratePhysicalDevices ().First ();
		}

	}
	
}
