﻿using System;
using System.Collections.Generic;
using Vulkan;

namespace Gfx.Vk
{
	interface IVulkanRenderTarget : IRenderTarget
	{
		Framebuffer CreateFramebuffer (RenderPass renderPass);
	}


	class Factory : IPlatformFactory
	{
		readonly Context _Context;

		public Factory (Context context)
		{
			this._Context = context;
		}


		public RadialGradient CreateRadialGradientBrush ()
		{
			return new VkRadialGradientBrush ();
		}

		public Geometry CreateGeometry ()
		{
			return new VkGeometry ();
		}

		public SolidBrush CreateSolidBrush ()
		{
			return new VkSolidBrush ();
		}

		public LinearGradient CreateLinearGradientBrush ()
		{
			return new VkLinearGradientBrush ();
		}

		public Bitmap CreateBitmap ()
		{
			return new VkBitmap (_Context);
		}
	}


	public class VulkanPlatform : IPlatform, IDisposable
	{
		readonly Context _Context;

		readonly Factory _Factory;
		readonly Renderer _Renderer;


		public VulkanPlatform (List<string> instanceExtentions)
		{
			_Context = new Context (true, instanceExtentions);
			_Renderer = new Renderer (_Context);

			_Factory = new Gfx.Vk.Factory (_Context);
		}

		public IRenderTarget CreateScreenRenderTarget (Func<Instance, SurfaceKhr> createCallback, bool vsync)
		{
			return new Swapchain (_Context, createCallback (_Context.Instance), 
				vsync ? PresentModeKhr.Fifo : PresentModeKhr.Immediate);
		}

		public IPlatformFactory Factory ()
		{
			return _Factory;
		}

		public void Dispose ()
		{
			_Context.Dispose ();
		}

		public IRenderer Renderer ()
		{
			return _Renderer;
		}
	}
}

