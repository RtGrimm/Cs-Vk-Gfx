using System;
using Gfx.Vk;
using Vulkan;
using System.IO;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.Generic;

namespace Gfx
{

	abstract class AggregateBuffer<Container, Item> : IDisposable where Container : InvalidationBase
	{
		
		readonly Gfx.Vk.Buffer _Buffer;
		readonly Gfx.Vk.Buffer _StagingBuffer;
		readonly Context _Context;

		private HashSet<Container> _CurrentMembers = new HashSet<Container> ();

		public int BufferSize {
			get;
			set;
		}

		public struct Range
		{
			public int ItemOffset;
			public int Count;
		}

		public Dictionary<Container, Range> RangeMap {
			get;
			private set;
		} = new Dictionary<Container, Range>();

		public AggregateBuffer (Context context, Gfx.Vk.Buffer buffer)
		{
			this._Context = context;
			this._Buffer = buffer;
			this._StagingBuffer = new Gfx.Vk.Buffer (
				context, BufferUsageFlags.TransferSrc, 
				MemoryPropertyFlags.HostVisible | MemoryPropertyFlags.HostCoherent);
			
		}

		public Vulkan.Buffer Value {
			get {
				return _Buffer.Value;
			}
		}

		public abstract void CopyToBuffer (Gfx.Vk.Buffer target, int offset, Item values);

		public abstract Item GetItems (Container container);

		public abstract int SizeOfItem (Item item);

		public abstract int Count (Item item);

		public void Dispose ()
		{
			_Buffer.Dispose ();
		}

		void EnsureSize (List<Container> containers)
		{
			var totalSize = 0;

			foreach (var container in containers) {
				var item = GetItems (container);
				totalSize += Count (item) * SizeOfItem (item);
			}
			
			if (totalSize > BufferSize) {
				_Buffer.Create (totalSize);
				_StagingBuffer.Create (totalSize);
				BufferSize = totalSize;
			}
		}




		void UploadToStagingBuffer (List<Container> containers)
		{
			RangeMap.Clear ();

			var itemOffset = 0;

			foreach (var container in containers) {
				container.Validate ();

				var items = GetItems (container);

				CopyToBuffer (_StagingBuffer, itemOffset * SizeOfItem (items), items);

				RangeMap [container] = new Range {
					ItemOffset = itemOffset,
					Count = Count (items)
				};

				itemOffset += Count (items);
			}
		}

		public void Upload (List<Container> containers, CommandBuffer commandBuffer)
		{
			if (containers.Count == 0) {
				return;
			}

			bool upload = containers.Any (container => 
				!_CurrentMembers.Contains (container) || container.Invalid);

			if (!upload) {
				//return;
			}

			_CurrentMembers.Clear ();
			foreach (var container in containers) {
				_CurrentMembers.Add (container);
			}

			EnsureSize (containers);

			UploadToStagingBuffer (containers);

			commandBuffer.CmdCopyBuffer (_StagingBuffer.Value, _Buffer.Value, new BufferCopy[] {
				new BufferCopy {
					SrcOffset = 0,
					DstOffset = 0,
					Size = BufferSize
				}
			});
		}
	}

	class BrushBuffer : AggregateBuffer<Brush, BrushData>
	{
		public BrushBuffer (Context context, Gfx.Vk.Buffer buffer) : base (context, buffer)
		{
		}


		public override void CopyToBuffer (Gfx.Vk.Buffer target, int offset, BrushData values)
		{
			unsafe {
				target.Upload (&values, SizeOfItem (values), offset);
			}
		}

		public override BrushData GetItems (Brush container)
		{
			var brushData = new BrushData ();

			var solidBrush = container as VkSolidBrush;

			if (solidBrush != null) {
				brushData.SolidColor = solidBrush;
				return brushData;
			}

			var linearBrush = container as VkLinearGradientBrush;

			if (linearBrush != null) {
				brushData.LinearGradient = linearBrush;
				return brushData;
			}

			var radialBrush = container as VkRadialGradientBrush;

			if (radialBrush != null) {
				brushData.RadialGradient = radialBrush;
				return brushData;
			}

			throw new NotImplementedException ();
		}

		public override int SizeOfItem (BrushData item)
		{
			return Marshal.SizeOf<BrushData> ();
		}

		public override int Count (BrushData item)
		{
			return 1;
		}

	}

	class ShapeDataBuffer : AggregateBuffer<Shape, ShapeData>
	{
		public ShapeDataBuffer (Context context, Gfx.Vk.Buffer buffer) : base (context, buffer)
		{
		}


		public override void CopyToBuffer (Gfx.Vk.Buffer target, int offset, ShapeData values)
		{
			unsafe {
				target.Upload (&values, SizeOfItem (values), offset);
			}
		}

		public override ShapeData GetItems (Shape container)
		{
			ShapeData data;
			data.Transform = container.Transform;

			return data;
		}

		public override int SizeOfItem (ShapeData item)
		{
			return Marshal.SizeOf<ShapeData> ();
		}

		public override int Count (ShapeData item)
		{
			return 1;
		}
	}

	class VertexBuffer : AggregateBuffer<VkGeometry, Vertex[]>
	{
		public VertexBuffer (Context context, Gfx.Vk.Buffer buffer) : base (context, buffer)
		{
		}


		public override void CopyToBuffer (Gfx.Vk.Buffer target, int offset, Vertex[] values)
		{
			unsafe {
				fixed(void* ptr = values) {
					target.Upload (ptr, MemoryHelper.SizeOfArray<Vertex> (values), offset);
				}
			}
		}

		public override Vertex[] GetItems (VkGeometry container)
		{
			return container.Vertices;
		}

		public override int SizeOfItem (Vertex[] item)
		{
			return Marshal.SizeOf<Vertex> ();
		}

		public override int Count (Vertex[] item)
		{
			return item.Length;
		}
	}

	class IndexBuffer : AggregateBuffer<VkGeometry, uint[]>
	{
		public IndexBuffer (Context context, Gfx.Vk.Buffer buffer) : base (context, buffer)
		{
		}



		public override void CopyToBuffer (Gfx.Vk.Buffer target, int offset, uint[] values)
		{
			unsafe {
				fixed(void* ptr = values) {
					target.Upload (ptr, MemoryHelper.SizeOfArray<uint> (values), offset);
				}
			}
		}

		public override uint[] GetItems (VkGeometry container)
		{
			return container.Indices;
		}

		public override int SizeOfItem (uint[] item)
		{
			return Marshal.SizeOf<uint> ();
		}

		public override int Count (uint[] item)
		{
			return item.Length;
		}
	}
	
}
