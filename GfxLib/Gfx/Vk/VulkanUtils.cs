﻿using System;
using Vulkan;
using System.IO;

namespace Gfx.Vk
{
	class VulkanUtils
	{
		public const uint WholeSize = uint.MaxValue;

		public static uint FindMemoryTypeIndex (PhysicalDevice physicalDevice, MemoryPropertyFlags flags)
		{
			var props = physicalDevice.GetMemoryProperties ();

			for (uint i = 0; i < props.MemoryTypeCount; i++) {
				if (props.MemoryTypes [i].PropertyFlags == flags)
					return i;
			}

			throw new Exception ("Unable to locate matching memory type.");
		}

		public static ShaderModule LoadShaderFromFile (Device device, string path)
		{
			if (!File.Exists (path)) {
				throw new FileNotFoundException ();
			}

			var data = File.ReadAllBytes (path);

			return device.CreateShaderModule (new ShaderModuleCreateInfo {
				CodeSize = new UIntPtr ((ulong)data.Length),
				CodeBytes = data
			});
		}
	}
}

