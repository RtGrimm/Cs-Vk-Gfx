using System;
using System.Linq;
using Vulkan;

using Vulkan.Linux;

using System.Runtime.InteropServices;
using System.Reflection;
using System.Collections.Generic;
using System.IO;

namespace Gfx.Vk
{

	class Buffer : IDisposable
	{
		private readonly Context _Context;
		private readonly BufferUsageFlags _Usage;
		private readonly MemoryPropertyFlags _MemoryFlags;

		public Vulkan.Buffer Value {
			get;
			private set;
		}

		private DeviceMemory _BufferMemory;

		public int Size {
			get;
			set;
		}

		private IntPtr _Ptr = IntPtr.Zero;



		public Buffer (Context _Context, BufferUsageFlags _Usage, MemoryPropertyFlags _MemoryFlags)
		{
			this._Context = _Context;
			this._Usage = _Usage;
			this._MemoryFlags = _MemoryFlags;
		}

		public void Create (int size)
		{
			if (Value != null) {
				_Ptr = IntPtr.Zero;
				Dispose ();
			}

			Size = size;

			CreateBuffer ();
			AllocateMemory ();
		}

		private void CreateBuffer ()
		{
			Value = _Context.Device.CreateBuffer (new BufferCreateInfo {
				SharingMode = SharingMode.Exclusive,
				Size = Size,
				Usage = _Usage
			});
		}

		private void AllocateMemory ()
		{
			var index = VulkanUtils.FindMemoryTypeIndex (_Context.PhysicalDevice, _MemoryFlags);

			_BufferMemory = _Context.Device.AllocateMemory (new MemoryAllocateInfo {
				AllocationSize = _Context.Device.GetBufferMemoryRequirements (Value).Size,
				MemoryTypeIndex = (uint)index
			});

			_Context.Device.BindBufferMemory (Value, _BufferMemory, 0);
		}

		public void Upload (IntPtr data, int length, int offset)
		{
			unsafe {
				Upload (data.ToPointer (), length, offset);
			}
		}

		public unsafe void Upload (void* data, int length, int offset)
		{
			if (length + offset > Size) {
				throw new ArgumentOutOfRangeException (
					"Specified range is outside the bounds of the buffer.");
			}

			if (_Ptr == IntPtr.Zero) {
				_Ptr = _Context.Device.MapMemory (_BufferMemory, 0, Size);
			}

			MemoryHelper.Copy (IntPtr.Add (_Ptr, offset).ToPointer (), data, (uint)length);
		}

		public void Dispose ()
		{
			_Context.Device.DestroyBuffer (Value);
			_Context.Device.FreeMemory (_BufferMemory);
		}
	}
	
}
