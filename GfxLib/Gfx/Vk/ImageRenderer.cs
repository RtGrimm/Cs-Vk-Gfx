﻿using System;
using System.Collections.Generic;
using Vulkan;
using System.Runtime.InteropServices;
using System.Linq;

namespace Gfx.Vk
{
	[StructLayout (LayoutKind.Sequential)]
	struct ImageData
	{
		public OpenTK.Matrix4 Transform;
		public int ImageIndex;
		int _Padding, __Padding, ___Padding;

		public ImageData (OpenTK.Matrix4 transform, int imageIndex)
		{
			this.Transform = transform;
			this.ImageIndex = imageIndex;

			_Padding = 0;
			__Padding = 0;
			___Padding = 0;
		}
	}


	class ImageDataBuffer : AggregateBuffer<Image, ImageData>
	{
		Dictionary<Image, int> _ImageIndexMap;

		public ImageDataBuffer (Dictionary<Image, int> imageIndexMap, Context context, Gfx.Vk.Buffer buffer) : base (context, buffer)
		{
			this._ImageIndexMap = imageIndexMap;
		}


		public override void CopyToBuffer (Gfx.Vk.Buffer target, int offset, ImageData values)
		{
			unsafe {
				target.Upload (&values, SizeOfItem (values), offset);
			}
		}

		public override ImageData GetItems (Image container)
		{
			ImageData data = new ImageData (container.Transform, _ImageIndexMap [container]);
			return data;
		}

		public override int SizeOfItem (ImageData item)
		{
			return Marshal.SizeOf<ImageData> ();
		}

		public override int Count (ImageData item)
		{
			return 1;
		}
	}

	class VkBitmap : Bitmap
	{
		public byte[] ImageData {
			get;
			set;
		}

		public int ByteSize {
			get;
			set;
		}

		public ImageView View {
			get;
			private set;
		}

		public Sampler Sampler {
			get;
			set;
		}

		Vulkan.Image _Image;
		DeviceMemory _ImageMemory;

		readonly Context _Context;

		public VkBitmap (Context context)
		{
			this._Context = context;
		}


		public override void Dispose ()
		{
			if (_Image == null) {
				return;
			}

			_Context.Device.DestroyImage (_Image);
			_Context.Device.DestroyImageView (View);
			_Context.Device.DestroySampler (Sampler);
		}

		public override void Update (int width, int height, byte[] data)
		{
			base.Update (width, height, data);
			Dispose ();

			_Image = _Context.Device.CreateImage (new ImageCreateInfo {
				ArrayLayers = 1,
				Extent = new Extent3D {
					Width = (uint)width,
					Height = (uint)height,
					Depth = 1
				},
				Format = Format.B8g8r8a8Srgb,
				ImageType = ImageType.Image2D,
				InitialLayout = ImageLayout.Preinitialized,
				MipLevels = 1,
				Samples = SampleCountFlags.Count1,
				Tiling = ImageTiling.Linear,
				Usage = ImageUsageFlags.Sampled
			});

			var requirements = _Context.Device
				.GetImageMemoryRequirements (_Image);

			_ImageMemory = _Context.Device.AllocateMemory (new MemoryAllocateInfo {
				AllocationSize = requirements.Size,
				MemoryTypeIndex = VulkanUtils.FindMemoryTypeIndex (_Context.PhysicalDevice, 
					MemoryPropertyFlags.HostCoherent | MemoryPropertyFlags.HostVisible)
			});


			unsafe {
				void* ptr = _Context.Device
					.MapMemory (_ImageMemory, 0, requirements.Size).ToPointer ();	

				fixed(void* dataPtr = data) {
					MemoryHelper.Copy (ptr, dataPtr, (uint)data.Length);
				}

				_Context.Device.UnmapMemory (_ImageMemory);
			}

			_Context.Device.BindImageMemory (_Image, _ImageMemory, 0);

			View = _Context.Device.CreateImageView (new ImageViewCreateInfo {

				Format = Format.B8g8r8a8Srgb,
				Image = _Image,
				SubresourceRange = new ImageSubresourceRange {
					AspectMask = ImageAspectFlags.Color,
					BaseArrayLayer = 0,
					BaseMipLevel = 0,
					LayerCount = 1,
					LevelCount = 1
				},
				ViewType = ImageViewType.View2D
			});

			Sampler = _Context.Device.CreateSampler (new SamplerCreateInfo {
				AddressModeU = SamplerAddressMode.ClampToEdge,
				AddressModeV = SamplerAddressMode.ClampToEdge,
				AddressModeW = SamplerAddressMode.ClampToEdge,

				MagFilter = Filter.Linear,
				MinFilter = Filter.Linear,
				
			});
		}
	}

	class ImageRenderer
	{
		readonly Context _Context;

		Buffer _MeshBuffer;
		bool _Init = false;

		Pipeline _Pipeline;

		PipelineLayout _PipelineLayout;

		DescriptorPool _DescriptorPool;
		DescriptorSetLayout _DescriptorLayout;

		DescriptorSet _DescriptorSet;

		const int MaxImageCount = 500;

		ImageDataBuffer _ImageDataBuffer;

		readonly Dictionary<Image, int> _ImageIndexMap = new Dictionary<Image, int> ();


		public ImageRenderer (Context context, RenderPass renderPass)
		{
			this._Context = context;
			_MeshBuffer = new Buffer (
				_Context, BufferUsageFlags.VertexBuffer | BufferUsageFlags.TransferDst, MemoryPropertyFlags.DeviceLocal);

			_ImageDataBuffer = new ImageDataBuffer (_ImageIndexMap, _Context, new Buffer (
				_Context, BufferUsageFlags.StorageBuffer | BufferUsageFlags.TransferDst, MemoryPropertyFlags.DeviceLocal));
			

			InitDescriptor ();
			CreatePipelineLayout ();
			CreatePipeline (renderPass);
		}

		void UpdateImageDescriptors (List<Image> images)
		{
			var bitmaps = images
				.Distinct ()
				.Select (image => (VkBitmap)image.Bitmap).ToList ();



			var imageInfoList = bitmaps.Distinct ().Select (bitmap => new DescriptorImageInfo {
				ImageLayout = ImageLayout.ShaderReadOnlyOptimal,
				ImageView = bitmap.View,
				Sampler = bitmap.Sampler
			}).ToArray ();

			_Context.Device.UpdateDescriptorSets (new[] {
				new WriteDescriptorSet {
					ImageInfo = imageInfoList,
					DescriptorCount = (uint)imageInfoList.Length,
					DescriptorType = DescriptorType.CombinedImageSampler,
					DstBinding = 1,
					DstSet = _DescriptorSet,
					
				}
			}, new CopyDescriptorSet[] { });
		}

		void UpdateDescriptor ()
		{
			_Context.Device.UpdateDescriptorSets (new[] {
				new WriteDescriptorSet {
					BufferInfo = new[] {
						new DescriptorBufferInfo () {
							Buffer = _ImageDataBuffer.Value,
							Offset = 0,
							Range = _ImageDataBuffer.BufferSize
						}
					},
					DescriptorCount = 1,
					DescriptorType = DescriptorType.StorageBuffer,
					DstBinding = 0,
					DstSet = _DescriptorSet
				}
			}, new CopyDescriptorSet[] { });
		}

		void InitDescriptor ()
		{
			_DescriptorLayout = _Context.Device.CreateDescriptorSetLayout (
				new DescriptorSetLayoutCreateInfo {
					BindingCount = 1,
					Bindings = new[] {
						new DescriptorSetLayoutBinding {
							Binding = 0,
							DescriptorCount = 1,
							DescriptorType = DescriptorType.StorageBuffer,
							StageFlags = ShaderStageFlags.Vertex
						},
						new DescriptorSetLayoutBinding {
							Binding = 1,
							DescriptorCount = MaxImageCount,
							DescriptorType = DescriptorType.CombinedImageSampler,
							StageFlags = ShaderStageFlags.Fragment
						}
					}
				});

			_DescriptorPool = _Context.Device.CreateDescriptorPool (new DescriptorPoolCreateInfo {
				MaxSets = 1,
				PoolSizes = new[] {
					new DescriptorPoolSize {
						DescriptorCount = 1,
						Type = DescriptorType.StorageBuffer
					},
					new DescriptorPoolSize {
						DescriptorCount = MaxImageCount,
						Type = DescriptorType.CombinedImageSampler
					}
				},
				PoolSizeCount = 2
			});

			_DescriptorSet = _Context.Device.AllocateDescriptorSets (new DescriptorSetAllocateInfo {
				DescriptorPool = _DescriptorPool,
				DescriptorSetCount = 1,
				SetLayouts = new[] { _DescriptorLayout }
			}) [0];
		}

		void CreatePipelineLayout ()
		{
			_PipelineLayout = _Context.Device
				.CreatePipelineLayout (new PipelineLayoutCreateInfo {
				SetLayouts = new[] { _DescriptorLayout }
			});
		}

		void CreatePipeline (RenderPass renderPass)
		{
			var vertexShader = VulkanUtils.LoadShaderFromFile (_Context.Device, "../../../Shaders/Image/vert.spv");
			var fragmentShader = VulkanUtils.LoadShaderFromFile (_Context.Device, "../../../Shaders/Image/frag.spv");

			var pipelineInfo = new PipelineBuilder ()
				.Attributes (new[] {
				new VertexInputAttributeDescription {
					Binding = 0,
					Format = Format.R32g32b32Sfloat,
					Location = 0
				},
				new VertexInputAttributeDescription {
					Binding = 0,
					Format = Format.R32g32Sfloat,
					Location = 1,
					Offset = (uint)(Marshal.SizeOf<float> () * 3)
				}
			}).Shaders (vertexShader, fragmentShader)
				.RenderPass (renderPass)
				.Layout (_PipelineLayout)
				.Topology (PrimitiveTopology.TriangleStrip)
				.Bindings (new[] {
				new VertexInputBindingDescription {
					Binding = 0,
					InputRate = VertexInputRate.Vertex,
					Stride = (uint)Marshal.SizeOf<Vertex> ()
				}
			}).Create ();

			_Pipeline = _Context.Device.CreateGraphicsPipelines (
				new PipelineCache (), 
				new[] { pipelineInfo }) [0];
		}

		private void UploadMesh (CommandBuffer commandBuffer)
		{
			Vertex[] vertices = new Vertex[] { 
				new Vertex (-1.0f, -1.0f, 0.0f, 0, 0),
				new Vertex (1.0f, -1.0f, 0.0f, 1.0f, 0.0f),
				new Vertex (-1.0f, 1.0f, 0.0f, 0.0f, 1.0f),
				new Vertex (1.0f, 1.0f, 0.0f, 1.0f, 1.0f)
			};

			var stagingBuffer = new Buffer (_Context, BufferUsageFlags.TransferSrc, 
				                    MemoryPropertyFlags.HostVisible | MemoryPropertyFlags.HostCoherent);

			var dataSize = MemoryHelper.SizeOfArray (vertices);

			stagingBuffer.Create (dataSize);


			unsafe {
				fixed(void* vertPtr = vertices) {
					stagingBuffer.Upload (vertPtr, dataSize, 0);
				}
			}

			_MeshBuffer.Create (dataSize);

			commandBuffer.CmdCopyBuffer (stagingBuffer.Value, _MeshBuffer.Value, new BufferCopy[] {
				new BufferCopy {
					Size = dataSize
				}
			});
		}

		void UpdateImageIndexMap (List<Image> images)
		{
			_ImageIndexMap.Clear ();

			var bitmaps = images.Select (image => (VkBitmap)image.Bitmap).ToList ();
			var indexMap = new Dictionary<VkBitmap, int> ();

			for (int i = 0; i < bitmaps.Count; i++) {
				indexMap [bitmaps [i]] = i;
			}

			foreach (var image in images) {
				_ImageIndexMap [image] = indexMap [(VkBitmap)image.Bitmap];
			}
		}

		public void PreRender (List<Image> images, CommandBuffer commandBuffer)
		{
			if (images.Count == 0) {
				return;
			}

			UpdateImageIndexMap (images);

			_ImageDataBuffer.Upload (images, commandBuffer);

			if (!_Init) {
				UploadMesh (commandBuffer);
				UpdateDescriptor ();

				_Init = true;
			}	

			UpdateImageDescriptors (images);
		}

		public void Render (List<Image> images, CommandBuffer commandBuffer)
		{
			if (images.Count == 0) {
				return;
			}

			commandBuffer.CmdBindPipeline (
				PipelineBindPoint.Graphics, _Pipeline);

			commandBuffer.CmdBindVertexBuffers (
				0, new[] { _MeshBuffer.Value }, new DeviceSize[] { 0 });


			commandBuffer.CmdBindDescriptorSets (PipelineBindPoint.Graphics,
				_PipelineLayout, 0, 
				new[] { _DescriptorSet }, new uint[] { });

			foreach (var image in images) {
				commandBuffer.CmdDraw (4, (uint)images.Count, 0, 0);
			}
		}
	}
}


