using System;
using Gfx.Vk;
using Vulkan;
using System.IO;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.Generic;

namespace Gfx
{
	class GeometryRenderInfo
	{
		public List<Shape> Shapes {
			get;
			set;
		}

		public CommandBuffer CommandBuffer {
			get;
			set;
		}
	}


	class GeometryRenderer : IDisposable
	{
		readonly Context _Context;
		readonly BrushManager _BrushManager;

		readonly VertexBuffer _VertexBuffer;
		readonly IndexBuffer _IndexBuffer;
		readonly ShapeDataBuffer _ShapeDataBuffer;
		readonly BrushBuffer _BrushBuffer;

		bool _DescriptorsUpdated = false;

		public GeometryRenderer (Context context, RenderPass renderPass)
		{
			this._Context = context;
			_BrushManager = new BrushManager (context, renderPass);

			var localFlags = MemoryPropertyFlags.DeviceLocal;

			_VertexBuffer = new VertexBuffer (_Context,
				new Gfx.Vk.Buffer (_Context, BufferUsageFlags.VertexBuffer | BufferUsageFlags.TransferDst, 
					localFlags));

			_IndexBuffer = new IndexBuffer (_Context,
				new Gfx.Vk.Buffer (_Context, BufferUsageFlags.IndexBuffer | BufferUsageFlags.TransferDst, 
					localFlags));

			_ShapeDataBuffer = new ShapeDataBuffer (_Context,
				new Gfx.Vk.Buffer (_Context, BufferUsageFlags.StorageBuffer | BufferUsageFlags.TransferDst, localFlags));

			_BrushBuffer = new BrushBuffer (_Context,
				new Gfx.Vk.Buffer (_Context, BufferUsageFlags.StorageBuffer | BufferUsageFlags.TransferDst, localFlags));
		}

		public void Dispose ()
		{
			_VertexBuffer.Dispose ();
			_IndexBuffer.Dispose ();
			_BrushManager.Dispose ();
		}

		void UpdateDescriptor ()
		{
			_Context.Device.UpdateDescriptorSets (new[] {
				new WriteDescriptorSet {
					BufferInfo = new[] {
						new DescriptorBufferInfo () {
							Buffer = _ShapeDataBuffer.Value,
							Offset = 0,
							Range = _ShapeDataBuffer.BufferSize
						}
					},
					DescriptorCount = 1,
					DescriptorType = DescriptorType.StorageBuffer,
					DstBinding = 0,
					DstSet = _BrushManager.DescriptorSet
				}
			}, new CopyDescriptorSet[] { });

			_Context.Device.UpdateDescriptorSets (new[] {
				new WriteDescriptorSet {
					BufferInfo = new[] {
						new DescriptorBufferInfo () {
							Buffer = _BrushBuffer.Value,
							Offset = 0,
							Range = _BrushBuffer.BufferSize
						}
					},
					DescriptorCount = 1,
					DescriptorType = DescriptorType.StorageBuffer,
					DstBinding = 1,
					DstSet = _BrushManager.DescriptorSet
				}
			}, new CopyDescriptorSet[] { });
		}

		void UploadGeometry (List<Shape> shapes, CommandBuffer commandBuffer)
		{
			var geometryList = shapes.Select 
				(shape => (VkGeometry)shape.Geometry).ToList ();

			var brushList = shapes
				.Select (shape => shape.Fill)
				.Where (brush => brush != null)
				.Distinct ()
				.ToList ();

			_VertexBuffer.Upload (geometryList, commandBuffer);
			_IndexBuffer.Upload (geometryList, commandBuffer);
			_ShapeDataBuffer.Upload (shapes, commandBuffer);
			_BrushBuffer.Upload (brushList, commandBuffer);


		}

		public void PreRender (List<Shape> shapes, CommandBuffer commandBuffer)
		{
			UploadGeometry (shapes, commandBuffer);

			if (!_DescriptorsUpdated) {
				UpdateDescriptor ();
				_DescriptorsUpdated = true;
			}
		}

		public void Render (GeometryRenderInfo renderInfo)
		{
			var commandBuffer = renderInfo.CommandBuffer;

			commandBuffer.CmdBindPipeline (
				PipelineBindPoint.Graphics, _BrushManager.Pipeline);
			
			commandBuffer.CmdBindVertexBuffers (
				0, new[] { _VertexBuffer.Value }, new DeviceSize[] { 0 });

			commandBuffer.CmdBindIndexBuffer (_IndexBuffer.Value, 0, IndexType.Uint32);
			commandBuffer.CmdBindDescriptorSets (PipelineBindPoint.Graphics,
				_BrushManager.PipelineLayout, 0, 
				new[] { _BrushManager.DescriptorSet }, new uint[] { });

			foreach (var shape in renderInfo.Shapes) {
				var geometry = (VkGeometry)shape.Geometry;

				unsafe {
					PushConstantData pushConstantData;
					pushConstantData.TransformIndex = 
						_ShapeDataBuffer.RangeMap [shape].ItemOffset;

					pushConstantData.BrushIndex =
						_BrushBuffer.RangeMap [shape.Fill].ItemOffset;

					commandBuffer.CmdPushConstants (
						_BrushManager.PipelineLayout, 
						ShaderStageFlags.Fragment | ShaderStageFlags.Vertex, 0, 
						(uint)Marshal.SizeOf<PushConstantData> (), 
						new IntPtr (&pushConstantData));
				}

				commandBuffer.CmdDrawIndexed (
					(uint)_IndexBuffer.RangeMap [geometry].Count, 1, 
					(uint)_IndexBuffer.RangeMap [geometry].ItemOffset, 
					_VertexBuffer.RangeMap [geometry].ItemOffset, 0);
			}
		}
	}
	
}
