using System;
using Gfx.Vk;
using Vulkan;
using System.IO;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.Generic;

namespace Gfx
{
	[StructLayout (LayoutKind.Sequential)]
	struct Vertex
	{
		Vec3 _Location;
		Vec2 _UV;

		public Vertex (float x, float y, float z, float u, float v)
		{
			_Location.X = x;
			_Location.Y = y;
			_Location.Z = z;
			_UV.X = u;
			_UV.Y = v;
		}

	}


	[StructLayout (LayoutKind.Sequential)]
	struct ShapeData
	{
		public OpenTK.Matrix4 Transform;
	}

	[StructLayout (LayoutKind.Sequential)]
	struct BrushData
	{
		const int MaxGradientStops = 4;
		const int ColorComponentCount = 4;

		unsafe fixed float Colors[MaxGradientStops * ColorComponentCount];
		unsafe fixed float Offsets[MaxGradientStops];

		Vec2 Vector1;
		Vec2 Vector2;

		int StopCount;
		int BrushType;

		int _Padding, __Padding;

		public unsafe VkSolidBrush SolidColor {
			set {
				fixed(float* ptr = Colors) {
					SetColor (0, ptr, value.Color);
				}

				BrushType = 0;
			}
		}

		unsafe void AssignGradientStops (GradientBrush value)
		{
			for (int i = 0; i < value.Stops.Count; i++) {
				fixed (float* ptr = Colors) {
					SetColor (i, ptr, value.Stops [i].Color);
				}
				fixed (float* ptr = Offsets) {
					ptr [i] = value.Stops [i].Offset;
				}
			}

			StopCount = value.Stops.Count;
		}

		public unsafe VkLinearGradientBrush LinearGradient {
			set {
				AssignGradientStops (value);

				Vector1 = value.Start;
				Vector2 = value.End;

				BrushType = 1;
			}
		}

		public unsafe VkRadialGradientBrush RadialGradient {
			set {
				AssignGradientStops (value);

				Vector1 = value.Center;
				Vector2 = value.Radius;

				BrushType = 2;
			}
		}

		private unsafe void SetColor (int index, float* ptr, Color color)
		{
			ptr [index * ColorComponentCount] = color.R;
			ptr [index * ColorComponentCount + 1] = color.G;
			ptr [index * ColorComponentCount + 2] = color.B;
			ptr [index * ColorComponentCount + 3] = color.A;
		}
	}

	[StructLayout (LayoutKind.Sequential)]
	struct PushConstantData
	{
		public int TransformIndex;
		public int BrushIndex;
	}

	class BrushManager : IDisposable
	{
		readonly Context _Context;

		public Pipeline Pipeline {
			get;
			private set;
		}

		public PipelineLayout PipelineLayout {
			get;
			private set;
		}

		DescriptorPool _DescriptorPool;
		DescriptorSetLayout _DescriptorLayout;

		public DescriptorSet DescriptorSet {
			get;
			private set;
		}

		private readonly RenderPass _RenderPass;

		public BrushManager (Context context, RenderPass renderPass)
		{
			this._RenderPass = renderPass;
			this._Context = context;

			InitDescriptor ();
			CreatePipelineLayout ();
			CreatePipeline ();
		}



		public void Dispose ()
		{
			_Context.Device.DestroyPipeline (Pipeline);
			_Context.Device.DestroyPipelineLayout (PipelineLayout);

			_Context.Device.FreeDescriptorSets (_DescriptorPool, new[] { DescriptorSet });
			_Context.Device.DestroyDescriptorSetLayout (_DescriptorLayout);
			_Context.Device.DestroyDescriptorPool (_DescriptorPool);
		}

		void CreatePipelineLayout ()
		{
			PipelineLayout = _Context.Device
				.CreatePipelineLayout (new PipelineLayoutCreateInfo {
				SetLayouts = new[] { _DescriptorLayout },
				SetLayoutCount = 1,
				PushConstantRangeCount = 1,
				PushConstantRanges = new[] {
					new PushConstantRange {
						Offset = 0,
						Size = (uint)Marshal.SizeOf<PushConstantData> (),
						StageFlags = ShaderStageFlags.Vertex | ShaderStageFlags.Fragment
					}
				}
			});
		}

		void InitDescriptor ()
		{
			_DescriptorLayout = _Context.Device.CreateDescriptorSetLayout (
				new DescriptorSetLayoutCreateInfo {
					BindingCount = 1,
					Bindings = new[] {
						new DescriptorSetLayoutBinding {
							Binding = 0,
							DescriptorCount = 1,
							DescriptorType = DescriptorType.StorageBuffer,
							StageFlags = ShaderStageFlags.Vertex
						},
						new DescriptorSetLayoutBinding {
							Binding = 1,
							DescriptorCount = 1,
							DescriptorType = DescriptorType.StorageBuffer,
							StageFlags = ShaderStageFlags.Fragment
						}
					}
				});

			_DescriptorPool = _Context.Device.CreateDescriptorPool (new DescriptorPoolCreateInfo {
				MaxSets = 1,
				PoolSizes = new[] {
					new DescriptorPoolSize {
						DescriptorCount = 2,
						Type = DescriptorType.StorageBuffer
					}
				},
				PoolSizeCount = 1
			});

			DescriptorSet = _Context.Device.AllocateDescriptorSets (new DescriptorSetAllocateInfo {
				DescriptorPool = _DescriptorPool,
				DescriptorSetCount = 1,
				SetLayouts = new[] { _DescriptorLayout }
			}) [0];
		}

		void CreatePipeline ()
		{
			var vertexShader = VulkanUtils.LoadShaderFromFile (_Context.Device, "../../../Shaders/Brush/vert.spv");
			var fragmentShader = VulkanUtils.LoadShaderFromFile (_Context.Device, "../../../Shaders/Brush/frag.spv");

			var info = new PipelineBuilder ().Attributes (new[] {
				new VertexInputAttributeDescription {
					Binding = 0,
					Format = Format.R32g32b32Sfloat,
					Location = 0
				},
				new VertexInputAttributeDescription {
					Binding = 0,
					Format = Format.R32g32Sfloat,
					Location = 1,
					Offset = (uint)(Marshal.SizeOf<float> () * 3)
				}
			}).Bindings (new[] {
				new VertexInputBindingDescription {
					Binding = 0,
					InputRate = VertexInputRate.Vertex,
					Stride = (uint)Marshal.SizeOf<Vertex> ()
				}
			})
				.RenderPass (_RenderPass)
				.Layout (PipelineLayout)
				.Shaders (vertexShader, fragmentShader)
				.Create ();

		

			Pipeline = _Context.Device.CreateGraphicsPipelines (new PipelineCache (), new[] {
				info
			}) [0];
		}
	}
	
}
