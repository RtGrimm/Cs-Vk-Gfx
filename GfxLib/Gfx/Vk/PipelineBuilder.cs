﻿using System;
using Vulkan;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Gfx.Vk
{
	public class PipelineBuilder
	{
		GraphicsPipelineCreateInfo _PipelineInfo;

		public PipelineBuilder ()
		{
			InitDefaults ();
		}

		public PipelineBuilder Bindings (IEnumerable<VertexInputBindingDescription> bindings)
		{
			var bindingArray = bindings.ToArray ();

			_PipelineInfo.VertexInputState
				.VertexBindingDescriptions = bindingArray;
			
			_PipelineInfo.VertexInputState
				.VertexBindingDescriptionCount = (uint)bindingArray.Length;

			return this;
		}

		public PipelineBuilder Attributes (IEnumerable<VertexInputAttributeDescription> descList)
		{
			var descArray = descList.ToArray ();

			_PipelineInfo.VertexInputState
				.VertexAttributeDescriptions = descArray;
			
			_PipelineInfo.VertexInputState
				.VertexBindingDescriptionCount = (uint)descArray.Length;

			return this;
		}


		public PipelineBuilder Layout (PipelineLayout layout)
		{
			_PipelineInfo.Layout = layout;
			return this;
		}

		public PipelineBuilder RenderPass (RenderPass renderPass)
		{
			_PipelineInfo.RenderPass = renderPass;
			return this;
		}

		public PipelineBuilder Topology (PrimitiveTopology topology)
		{
			_PipelineInfo.InputAssemblyState.Topology = topology;
			return this;
		}

		public GraphicsPipelineCreateInfo Create ()
		{
			return _PipelineInfo;
		}

		public PipelineBuilder Shaders (ShaderModule vertexShader, ShaderModule fragmentShader)
		{
			_PipelineInfo.StageCount = 2;
			_PipelineInfo.Stages = new[] {
				new PipelineShaderStageCreateInfo () {
					Module = vertexShader,
					Name = "main",
					Stage = ShaderStageFlags.Vertex
				},
				new PipelineShaderStageCreateInfo () {
					Module = fragmentShader,
					Name = "main",
					Stage = ShaderStageFlags.Fragment
				}
			};

			return this;
		}


		void InitDefaults ()
		{
			_PipelineInfo = new GraphicsPipelineCreateInfo {
				ColorBlendState = new PipelineColorBlendStateCreateInfo {
					AttachmentCount = 1,
					Attachments = new[] {
						new PipelineColorBlendAttachmentState {
							BlendEnable = false,
							ColorWriteMask = ColorComponentFlags.R
							| ColorComponentFlags.G
							| ColorComponentFlags.B
							| ColorComponentFlags.A
						}
					}
				},
				DepthStencilState = new PipelineDepthStencilStateCreateInfo {
					
				},
				DynamicState = new PipelineDynamicStateCreateInfo {
					DynamicStateCount = 2,
					DynamicStates = new[] {
						DynamicState.Scissor,
						DynamicState.Viewport
					}
				},
				InputAssemblyState = new PipelineInputAssemblyStateCreateInfo {
					Topology = PrimitiveTopology.TriangleList
				},

				MultisampleState = new PipelineMultisampleStateCreateInfo {
					RasterizationSamples = SampleCountFlags.Count1
				},
				RasterizationState = new PipelineRasterizationStateCreateInfo {
					CullMode = CullModeFlags.None,
					FrontFace = FrontFace.Clockwise,
					LineWidth = 1,
					PolygonMode = PolygonMode.Fill
				},
				VertexInputState = new PipelineVertexInputStateCreateInfo { },
				ViewportState = new PipelineViewportStateCreateInfo {
					ScissorCount = 1,
					Scissors = new[] {
						new Rect2D {
							Extent = new Extent2D {
								Width = 1,
								Height = 1
							},
							Offset = new Offset2D ()
						}
					},
					ViewportCount = 1,
					Viewports = new[] {
						new Viewport {
							Height = 1,
							Width = 1,
							X = 0,
							Y = 0
						}
					}
				}
			};
		}
	}
}

