using System;
using Gfx.Vk;
using Vulkan;
using System.IO;
using System.Runtime.InteropServices;
using System.Linq;
using System.Collections.Generic;

namespace Gfx
{
	class VkSolidBrush : SolidBrush
	{
	}

	class VkLinearGradientBrush : LinearGradient
	{
		
	}

	class VkRadialGradientBrush : RadialGradient
	{
		
	}

	class VkGeometry : Geometry
	{
		

		public Vertex[] Vertices {
			get;
			private set;
		}

		public uint[] Indices {
			get;
			set;
		}

		protected override void GenerateInternalData (Vec3[] vertices, Vec2[] uv, uint[] indices)
		{
			Vertices = vertices
				.Zip (uv, (x, y) => Tuple.Create (x, y))
				.Select (pair => 
					new Vertex (
				pair.Item1.X, pair.Item1.Y, pair.Item1.Z, 
				pair.Item2.X, pair.Item2.Y)).ToArray ();
		
			Indices = indices;
		}

		
	}

	class Renderer : IRenderer, IDisposable
	{
		readonly Context _Context;
		readonly GeometryRenderer _GeometryRenderer;
		readonly ImageRenderer _ImageRenderer;

		RenderPass _RenderPass;
		CommandPool _CommandPool;
		CommandBuffer _DrawCommandBuffer;

		Queue _Queue;


		public Renderer (Context context)
		{
			this._Context = context;
			CreateRenderPass ();
			this._GeometryRenderer = new GeometryRenderer (context, _RenderPass);
			this._ImageRenderer = new ImageRenderer (context, _RenderPass);
			InitCommandBuffer ();

			_Queue = _Context.Device.GetQueue (_Context.RenderQueueFamilyIndex, 0);
		}

		public void Dispose ()
		{
			_GeometryRenderer.Dispose ();
			_Context.Device.DestroyRenderPass (_RenderPass);
		}

		void InitCommandBuffer ()
		{
			_CommandPool = _Context.Device.CreateCommandPool (new CommandPoolCreateInfo {
				QueueFamilyIndex = _Context.RenderQueueFamilyIndex,
				Flags = CommandPoolCreateFlags.ResetCommandBuffer
			});

			_DrawCommandBuffer = _Context.Device.AllocateCommandBuffers (
				new CommandBufferAllocateInfo {
					CommandBufferCount = 1,
					CommandPool = _CommandPool,
					Level = CommandBufferLevel.Primary
				}) [0];
		}

		void CreateRenderPass ()
		{
			_RenderPass = _Context.Device.CreateRenderPass (new RenderPassCreateInfo {
				AttachmentCount = 1,
				Attachments = new[] {
					new AttachmentDescription {
						FinalLayout = ImageLayout.PresentSrcKhr,
						InitialLayout = ImageLayout.Undefined,
						LoadOp = AttachmentLoadOp.Clear,
						StoreOp = AttachmentStoreOp.Store,
						Samples = SampleCountFlags.Count1,
						StencilLoadOp = AttachmentLoadOp.DontCare,
						StencilStoreOp = AttachmentStoreOp.DontCare,
						Format = Format.B8g8r8a8Unorm
					}
				},
				SubpassCount = 1,
				Subpasses = new [] {
					new SubpassDescription {
						ColorAttachmentCount = 1,
						ColorAttachments = new[] {
							new AttachmentReference {
								Attachment = 0,
								Layout = ImageLayout.ColorAttachmentOptimal
							}
						}
					}
				}
			});
		}

		void BeginRender (RenderInfo renderInfo, int width, int height, Framebuffer framebuffer)
		{
			_DrawCommandBuffer.CmdBeginRenderPass (new RenderPassBeginInfo {
				ClearValueCount = 1,
				ClearValues = new[] {
					new ClearValue {
						Color = new ClearColorValue (new[] {
							renderInfo.ClearColor.R,
							renderInfo.ClearColor.G,
							renderInfo.ClearColor.B,
							renderInfo.ClearColor.A
						}),
					}
				},
				Framebuffer = framebuffer,
				RenderArea = new Rect2D {
					Extent = new Extent2D {
						Width = (uint)width,
						Height = (uint)height
					},
					Offset = new Offset2D ()
				},
				RenderPass = _RenderPass
			}, SubpassContents.Inline);
			_DrawCommandBuffer.CmdSetScissor (0, new[] {
				new Rect2D {
					Offset = new Offset2D (),
					Extent = new Extent2D {
						Width = (uint)width,
						Height = (uint)height
					}
				}
			});
			_DrawCommandBuffer.CmdSetViewport (0, new[] {
				new Viewport {
					X = 0,
					Y = 0,
					Width = (float)width,
					Height = (float)height,
					MinDepth = 0,
					MaxDepth = 1
				}
			});
		}

		void EndRender ()
		{
			_DrawCommandBuffer.CmdEndRenderPass ();
			_DrawCommandBuffer.End ();
		}

		void Draw (RenderInfo renderInfo, int width, int height, Framebuffer framebuffer)
		{
			_DrawCommandBuffer.Reset ();

			_DrawCommandBuffer.Begin (new CommandBufferBeginInfo {
				InheritanceInfo = new CommandBufferInheritanceInfo ()
			});

			var geometryRenderInfo = new GeometryRenderInfo () {
				CommandBuffer = _DrawCommandBuffer,
				Shapes = renderInfo.RenderObjects.OfType<Shape> ().ToList ()
			};

			var images = renderInfo.RenderObjects.OfType<Image> ().ToList ();

			_GeometryRenderer.PreRender (geometryRenderInfo.Shapes, _DrawCommandBuffer);
			_ImageRenderer.PreRender (images, _DrawCommandBuffer);

			BeginRender (renderInfo, width, height, framebuffer);

		
			_GeometryRenderer.Render (geometryRenderInfo);
			_ImageRenderer.Render (images, _DrawCommandBuffer);


			EndRender ();
		}

		void PresentSwapchain (RenderInfo renderInfo)
		{
			var width = renderInfo.Target.Width;
			var height = renderInfo.Target.Height;

			var swapchain = (Swapchain)renderInfo.Target;
			var target = (IVulkanRenderTarget)renderInfo.Target;

			var imageReadySemaphore = _Context.Device.CreateSemaphore (new SemaphoreCreateInfo ());
			var renderDoneSemaphore = _Context.Device.CreateSemaphore (new SemaphoreCreateInfo ());

			swapchain.NextImage (imageReadySemaphore);
			var framebuffer = target.CreateFramebuffer (_RenderPass);

			Draw (renderInfo, width, height, framebuffer);

			_Queue.Submit (new[] {
				new SubmitInfo {
					CommandBufferCount = 1,
					CommandBuffers = new[] {
						_DrawCommandBuffer
					},
					SignalSemaphoreCount = 1,
					SignalSemaphores = new[] {
						renderDoneSemaphore
					},
					WaitDstStageMask = new[] {
						PipelineStageFlags.Transfer
					},
					WaitSemaphoreCount = 1,
					WaitSemaphores = new[] {
						imageReadySemaphore
					}
				}
			}, new Fence ());

			swapchain.Present (_Queue, renderDoneSemaphore);

			_Queue.WaitIdle ();

			_Context.Device.DestroySemaphore (imageReadySemaphore);
			_Context.Device.DestroySemaphore (renderDoneSemaphore);

		}

		public void Render (RenderInfo renderInfo)
		{
			if (renderInfo.Target is Swapchain) {
				PresentSwapchain (renderInfo);
			}
		}
	}
}

