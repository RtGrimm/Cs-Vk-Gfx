using System;
using System.Linq;
using Vulkan;

using Vulkan.Linux;

using System.Runtime.InteropServices;
using System.Reflection;
using System.Collections.Generic;
using System.IO;

namespace Gfx.Vk
{

	class Swapchain : IDisposable, IVulkanRenderTarget
	{
		private SwapchainKhr _Swapchain = null;

		private readonly Context _Context;
		private readonly SurfaceKhr _Surface;
		private readonly PresentModeKhr _DesiredPresentMode;

		public int ImageIndex {
			get;
			private set;
		}

		public List<Vulkan.Image> Images {
			get;
			private set;
		}

		public List<ImageView> ImageViews {
			get;
			private set;
		} = new List<ImageView>();

		public int Width {
			get;
			private set;
		}

		public int Height {
			get;
			private set;
		}

		public Swapchain (Context _Context, SurfaceKhr _Surface, PresentModeKhr _DesiredPresentMode)
		{
			this._Context = _Context;
			this._Surface = _Surface;
			this._DesiredPresentMode = _DesiredPresentMode;

			Create ();
		}






		public void Resize (int width, int height)
		{
			Create ();
		}

		public SwapchainKhr Get ()
		{
			return _Swapchain;
		}

		public Framebuffer CreateFramebuffer (RenderPass renderPass)
		{
			return _Context.Device.CreateFramebuffer (new FramebufferCreateInfo {
				AttachmentCount = 1,
				Attachments = new[] { ImageViews [ImageIndex] },
				Width = (uint)Width,
				Height = (uint)Height,
				Layers = 1,
				RenderPass = renderPass
			});
		}

		public void Create ()
		{
			var capabilities = _Context.PhysicalDevice.GetSurfaceCapabilitiesKHR (_Surface);

			var format = GetFormat ();
			var presentMode = GetPresentMode ();

			var createInfo = new SwapchainCreateInfoKhr {
				Clipped = true,
				CompositeAlpha = CompositeAlphaFlagsKhr.Opaque,
				Flags = 0,
				ImageArrayLayers = 1,
				ImageColorSpace = format.Item2,
				ImageExtent = capabilities.CurrentExtent,
				ImageFormat = format.Item1,
				ImageSharingMode = SharingMode.Exclusive,
				ImageUsage = ImageUsageFlags.ColorAttachment,
				MinImageCount = Math.Min (capabilities.MaxImageCount, capabilities.MinImageCount + 1),
				PresentMode = presentMode,
				PreTransform = capabilities.CurrentTransform,
				Surface = _Surface
			};

			var oldSwapchain = _Swapchain;

			if (_Swapchain != null) {
				createInfo.OldSwapchain = oldSwapchain;
				DestroyImageViews ();
			}

			Width = (int)capabilities.CurrentExtent.Width;
			Height = (int)capabilities.CurrentExtent.Height;

			_Swapchain = _Context.Device.CreateSwapchainKHR (createInfo);

			if (oldSwapchain != null) {
				_Context.Device.DestroySwapchainKHR (oldSwapchain);	
			}


			Images = _Context.Device.GetSwapchainImagesKHR (_Swapchain).ToList ();
			CreateImageViews ();
		}

		public void Dispose ()
		{
			DestroyImageViews ();
			_Context.Device.DestroySwapchainKHR (_Swapchain);
			_Context.Instance.DestroySurfaceKHR (_Surface);


		}

		private void DestroyImageViews ()
		{
			foreach (var view in ImageViews) {
				_Context.Device.DestroyImageView (view);
			}

			ImageViews.Clear ();


		}

		public void NextImage (Semaphore waitSemaphore)
		{
			ImageIndex = (int)(_Context.Device.AcquireNextImageKHR (
				_Swapchain, ulong.MaxValue, waitSemaphore, new Fence ()));
		}


		public void Present (Queue queue, Semaphore waitSemaphore)
		{
			queue.PresentKHR (new PresentInfoKhr {
				ImageIndices = new[] {
					(uint)ImageIndex
				},
				SwapchainCount = 1,
				Swapchains = new[] {
					_Swapchain
				},
				WaitSemaphoreCount = 1,
				WaitSemaphores = new[] {
					waitSemaphore
				}
			});
		}

		private PresentModeKhr GetPresentMode ()
		{
			var presentModes = _Context.PhysicalDevice.GetSurfacePresentModesKHR (_Surface);	

			foreach (var presentMode in presentModes) {	
				if (presentMode == _DesiredPresentMode)
					return _DesiredPresentMode;
			}

			return PresentModeKhr.Fifo;
		}

		private Tuple<Format, ColorSpaceKhr> GetFormat ()
		{
			var formats = _Context.PhysicalDevice.GetSurfaceFormatsKHR (_Surface);

			var colorSpace = formats [0].ColorSpace;

			if (formats.Length == 1 && formats [0].Format == Format.Undefined) {
				return Tuple.Create (Format.R8g8b8a8Unorm, colorSpace);
			}

			return Tuple.Create (formats [0].Format, colorSpace);
		}

		private void CreateImageViews ()
		{
			foreach (var image in Images) {
				var imageView = _Context.Device.CreateImageView (new ImageViewCreateInfo {
					Format = Format.B8g8r8a8Unorm,
					Image = image,
					SubresourceRange = new ImageSubresourceRange {
						AspectMask = Vulkan.ImageAspectFlags.Color,
						BaseArrayLayer = 0,
						BaseMipLevel = 0,
						LayerCount = 1,
						LevelCount = 1
					},
					ViewType = ImageViewType.View2D
				});

				ImageViews.Add (imageView);
			}
		}
	}
	
}
