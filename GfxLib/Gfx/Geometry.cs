using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using LibTessDotNet;
using System.Linq;
using OpenTK;
using ClipperLib;

namespace Gfx
{

	public enum WindingRule
	{
		EvenOdd,
		NonZero,
		Positive,
		Negative,
		AbsGeqTwo
	}

	public enum JoinType
	{
		Square,
		Round,
		Miter
	}



	public enum EndType
	{
		ClosedPolygon,
		ClosedLine,
		OpenButt,
		OpenSquare,
		OpenRound
	}

	public class TessellationInfo
	{
		public WindingRule WindingRule {
			get;
			set;
		} = WindingRule.NonZero;

		public EndType EndType {
			get;
			set;
		} = EndType.ClosedLine;

		public JoinType JoinType {
			get;
			set;
		} = JoinType.Round;

		public float StrokeWidth {
			get;
			set;
		} = 1;

		public bool IsStroke {
			get;
			set;
		}
	}

	public class Polyline
	{
		public List<Vec2> Points {
			get;
			set;
		} = new List<Vec2>();
	}

	public class Path
	{
		public List<Polyline> Polylines {
			get;
			set;
		}
	}

	class PathGenerator
	{
		const float ClipperScaleFactor = 100000;

		static Tuple<Vec2, Vec2> CalcBoundingBox (IEnumerable<Vec3> points)
		{
			var minX = points.Min (point => point.X);
			var minY = points.Min (point => point.Y);

			var maxX = points.Max (point => point.X);
			var maxY = points.Max (point => point.Y);

			return Tuple.Create (
				new Vec2 (minX, minY), 
				new Vec2 (maxX - minX, maxY - minY));
		}

		static long ToClipperValue (float x)
		{
			return (long)(x * ClipperScaleFactor);
		}

		static float FromClipperValue (long x)
		{
			return (float)x / ClipperScaleFactor;
		}

		public static Path GenerateStroke (Path path, TessellationInfo tesselationInfo)
		{
			var clipper = new ClipperLib.ClipperOffset ();

			var intPaths = path.Polylines.Select (polyline => 
				polyline.Points.Select (point => 
					new IntPoint (
				               ToClipperValue (point.X), 
				               ToClipperValue (point.Y))).ToList ()).ToList ();

			clipper.AddPaths (intPaths, 
				(ClipperLib.JoinType)tesselationInfo.JoinType, 
				(ClipperLib.EndType)tesselationInfo.EndType);

			List<List<IntPoint>> result = new List<List<IntPoint>> ();
			clipper.Execute (ref result, (double)tesselationInfo.StrokeWidth * ClipperScaleFactor);

			return new Path {
				Polylines = result.Select (resultPolyline => 
					new Polyline {
					Points = resultPolyline.Select (
						point => new Vec2 (FromClipperValue (point.X), FromClipperValue (point.Y))).ToList ()
				}).ToList ()
			};
		}

		public static void Tesselate (Path path, TessellationInfo tesselationInfo, out Vec3[] vertices, out Vec2[] uvs, out uint[] elements)
		{
			
			LibTessDotNet.Tess tess = new Tess ();

			foreach (Polyline polyline in path.Polylines) {
				tess.AddContour (polyline.Points.Select (point => new ContourVertex {
					Position = new LibTessDotNet.Vec3 {
						X = point.X,
						Y = point.Y,
						Z = 0
					}
				}).ToArray ());
			}


			tess.Tessellate ((LibTessDotNet.WindingRule)tesselationInfo.WindingRule, ElementType.Polygons, 3);

			vertices = tess.Vertices.Select (vertex => 
				new Vec3 (vertex.Position.X, vertex.Position.Y, vertex.Position.Z)).ToArray ();

			var bbox = CalcBoundingBox (vertices);

			uvs = tess.Vertices.Select (vertex => new Vec2 (
				(vertex.Position.X - bbox.Item1.X) / bbox.Item2.X, 
				(vertex.Position.Y - bbox.Item1.Y) / bbox.Item2.Y)).ToArray ();

			elements = tess.Elements.Select (i => (uint)i).ToArray ();
		}
	}

	public abstract class Geometry : InvalidationBase
	{



		public void Tesselate (Path path)
		{
			Tesselate (path, new TessellationInfo ());
		}

		public void Tesselate (Path path, TessellationInfo tesselationInfo)
		{
			Invalidate ();

			Vec2[] uvs;
			uint[] elements;
			Vec3[] vertices;

			var targetPath = path;

			if (tesselationInfo.IsStroke) {
				targetPath = PathGenerator.GenerateStroke (path, tesselationInfo);
			}

			PathGenerator.Tesselate (
				targetPath, tesselationInfo, out vertices, out uvs, out elements);
			
			GenerateInternalData (vertices, uvs, elements);
		}

		protected abstract void GenerateInternalData (Vec3[] vertices, Vec2[] uv, uint[] indices);
	}

}
